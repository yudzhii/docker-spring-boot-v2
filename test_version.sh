ip_pod_new_version=''
while [ -z "$ip_pod_new_version"  ] ; do 
	ip_pod_new_version=$(kubectl  get pods -o go-template='{{range $index, $element := .items}}{{range .status.containerStatuses}}{{if .ready}}{{$element.status.podIP}}   {{.image}} {{"\n"}}   {{end}}{{end}}{{end}}'  |grep docker-spring-boot:CI_COMMIT_TAG |head -n 1| awk '{print  $1 ":8085/"}');

done ; curl $ip_pod_new_version > version
