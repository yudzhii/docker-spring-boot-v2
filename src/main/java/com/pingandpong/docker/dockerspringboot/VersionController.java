package com.pingandpong.docker.dockerspringboot;

import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.RequestMapping;

    @RestController
    public class VersionController {

        @RequestMapping("/")
        public String index() {
            return "v-2.2";
        }

    }
